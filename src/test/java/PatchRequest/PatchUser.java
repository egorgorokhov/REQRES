package PatchRequest;



import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PatchUser {
    @Test
    @DisplayName("Вызов метода /PATCH. Обновление данных пользователя")
    public void successPatchUser() throws InterruptedException, IOException, URISyntaxException{
        byte[] out = "{\"name\":\"solomon\",\"job\":\"king\"}".getBytes(StandardCharsets.UTF_8);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/users/3"))
                .method("PATCH", HttpRequest.BodyPublishers.ofByteArray(out))
                .build();

        HttpResponse response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());

        System.out.println(response.body());

        assertEquals(200, response.statusCode());

    }
}
